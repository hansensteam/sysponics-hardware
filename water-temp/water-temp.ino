#include <stdio.h>
#include <Sysponics.h>
#include <NodeMCU.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <DHT.h>
#include <string>



#define DATALED D1
#define LEVELLED D2
#define ONE_WIRE_BUS D3
#define TH_DATALED D5
#define DHTPIN D6
#define LEVEL_SENSOR D7

#define DHTTYPE DHT22

//ACCESS
#define USER_ID "wdltdfrmbgmhnfaa"
#define SENSOR_ID "edotjgyzqrmrclwx"
#define LEVEL_SENSOR_ID "18fa6fd5b129488a"
#define DHT_SENSOR_ID "rszkhajoldyccmyw"



long l_delay = 10000;
int last_level_state = -1;

DHT dht(DHTPIN, DHTTYPE);
OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);
void setup(void) {
  Serial.begin(9600);
  reconnectWifi( getWiFiData() );
  Serial.println( WiFi.localIP() );
  mqttSetup( getMQTTData() );
  pinMode( DATALED, OUTPUT );
  pinMode( LEVELLED, OUTPUT );
  pinMode( TH_DATALED, OUTPUT );
  pinMode( LEVEL_SENSOR, INPUT_PULLUP );
  sensors.begin();
  dht.begin();
}

void loop(void) {
  digitalWrite( DATALED, HIGH );
  digitalWrite( TH_DATALED, HIGH );
  if ( !getMQTT().connected()) {
    reconnectMQTT( getMQTTData() );
  }

  getMQTT().loop();

  sensors.requestTemperatures();
  int x = digitalRead( LEVEL_SENSOR );
  if ( x != last_level_state ) {
    last_level_state = x;
    Serial.println( last_level_state );
    if ( x == 0 ) {
      digitalWrite( LEVELLED, LOW );
    } else {
      digitalWrite( LEVELLED, HIGH );
    }
    LevelSensor lv;
    lv.setLow( x );
    lv.setUserId( USER_ID );
    lv.setSensorId( LEVEL_SENSOR_ID );
    sendMessage( lv.toJson(), data_topic() );
  }

  Serial.println( sensors.getTempCByIndex(0) );
  Serial.println( last_level_state );

  TempSensor temp;
  temp.setTemperature( sensors.getTempCByIndex(0) );
  temp.setUserId( USER_ID );
  temp.setSensorId( SENSOR_ID );
  sendMessage( temp.toJson(), data_topic() );




  TempHumSensor thSensor;
  thSensor.setSensorId( DHT_SENSOR_ID );
  thSensor.setUserId( USER_ID );
  thSensor.setTemperature( dht.readTemperature() );
  thSensor.setHumidity( dht.readHumidity() );

  Serial.println( dht.readTemperature() );
  Serial.println( dht.readHumidity() );
  

  sendMessage(thSensor.toJson(), data_topic() );
  digitalWrite( DATALED, LOW );
  digitalWrite( TH_DATALED, LOW );
  delay(l_delay);
}
std::string userId() {
  return USER_ID;
}
std::string sensorId() {
  return SENSOR_ID;
}

WifiConnectionData getWiFiData() {
  WifiConnectionData data;
  data.setSSID( "Hansen_Rep" );
  data.setPassword( "nicoli2010" );
  return data;
}

MQTTConnectionData getMQTTData() {

  //  data.setApiKey( USER_ID );
  //  data.setSensorType( WATER_TEMP_SENSOR );
  //  data.setSensorId( SENSOR_ID );
  std::string idf = userId() + "@";
  idf += WiFi.localIP().toString().c_str();
  Serial.println( idf.c_str() );
  //  data.setIdentifier( idf.c_str() );
  MQTTConnectionData data(USER_ID, WATER_TEMP_SENSOR, SENSOR_ID, idf );
  return data;
}

void mqtt_callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  Serial.println();
  Serial.println("-----------------------");
}
