#include <stdio.h>
#include <Sysponics.h>
#include <NodeMCU.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>

#define IDENTIFIER "20201115"
#define BROKER "192.168.2.104"
#define BROKER_PORT 1883


#define R1_IN1 D1
#define R1_IN2 D2
#define R1_IN3 D3
#define R1_IN4 D4
#define R2_IN1 D5
#define R2_IN2 D6
#define R2_IN3 D7
#define R2_IN4 D8
//
#define USER_ID "wdltdfrmbgmhnfaa"
#define K1 "1cdd2e7b97134ce5"
#define K2 "b81319dde3cc476b"
#define K3 "90142e537f04415a"
#define K4 "0dd322a6640d430b"
#define K5 "2770213aebc14f1f"
#define K6 "c012de6bec2a4d1d"
#define K7 "c556ad2eefdc44be"

MQTTConnectionData wChiller( std::string( USER_ID ), std::string( ACT_WATER_CHILLER ), K1, std::string( IDENTIFIER ) );
MQTTConnectionData wHeater( std::string( USER_ID ), std::string( ACT_WATER_HEATER ), K2, std::string( IDENTIFIER ) );
MQTTConnectionData wPump( std::string( USER_ID ), std::string( ACT_WATER_PUMP ), K3, std::string( IDENTIFIER ) );
MQTTConnectionData lights( std::string( USER_ID ), std::string( ACT_LIGHTS ), K4, std::string( IDENTIFIER ) );
MQTTConnectionData aHeater( std::string( USER_ID ), std::string( ACT_AIR_HEATER ), K5, std::string( IDENTIFIER ) );
MQTTConnectionData aChiller( std::string( USER_ID ), std::string( ACT_AIR_CHILLER ), K6, std::string( IDENTIFIER ) );
MQTTConnectionData sprinkler( std::string( USER_ID ), std::string( ACT_SPRINKLER ), K7, std::string( IDENTIFIER ) );

int S1 = LOW;
int S2 = LOW;
int S3 = LOW;
int S4 = LOW;
int S5 = LOW;
int S6 = LOW;
int S7 = LOW;
int S8 = LOW;
long l_delay = 30000;

WiFiUDP ntpUDP;
NTPClient timeClient( ntpUDP, "pool.ntp.org" );

void subscribeAndConfig( MQTTConnectionData dados );
void subscribeAndConfig(  );

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode( R1_IN1, OUTPUT );
  pinMode( R1_IN2, OUTPUT );
  pinMode( R1_IN3, OUTPUT );
  pinMode( R1_IN4, OUTPUT );
  pinMode( R2_IN1, OUTPUT );
  pinMode( R2_IN2, OUTPUT );
  pinMode( R2_IN3, OUTPUT );
  pinMode( R2_IN4, OUTPUT );
  reconnectWifi( getWiFiData() );

  timeClient.begin();
  //TimeZone Offset - Em Segundos
  timeClient.setTimeOffset( 3600 * ( -3 ));

  digitalWrite( R1_IN1, HIGH );
  digitalWrite( R1_IN2, HIGH );
  digitalWrite( R1_IN3, HIGH );
  digitalWrite( R1_IN4, HIGH );
  digitalWrite( R2_IN1, HIGH );
  digitalWrite( R2_IN2, HIGH );
  digitalWrite( R2_IN3, HIGH );
  digitalWrite( R2_IN4, HIGH );

}

void loop() {
  // put your main code here, to run repeatedly:
  if ( !getMQTT().connected() ) {
    Serial.println("RECONNECT");
    reconnectMQTT( wChiller );
    subscribeAndConfig();
  }
  getMQTT().loop();

  sendState(K1, S1);
  sendState(K2, S2);
  sendState(K3, S3);
  sendState(K4, S4);
  sendState(K5, S5);
  sendState(K6, S6);
  sendState(K7, S7);

  delay(l_delay);
}

void sendState( std::string device, int state ) {
  std::string topic = "sysponics/user/wdltdfrmbgmhnfaa/data";
  int capacity = JSON_OBJECT_SIZE(3);
  DynamicJsonDocument doc(capacity);
  doc["sensor_id"] = device.c_str();
  doc["low"] = state;
  doc["user_id"] = USER_ID;
  sendMessage( doc, topic.c_str() );
}


WifiConnectionData getWiFiData() {
  WifiConnectionData data;
  data.setSSID( "Hansen_Rep" );
  data.setPassword( "nicoli2010" );
  return data;
}

std::string userId() {
  return USER_ID;
}

void subscribeAndConfig() {
  subscribeAndConfig( lights );
  subscribeToTopics( wHeater );
  subscribeToTopics( wPump );
  subscribeToTopics( wChiller );
  subscribeToTopics( aHeater );
  subscribeToTopics( aChiller );
  subscribeToTopics( sprinkler );
}

void subscribeAndConfig( MQTTConnectionData dados ) {
  subscribeToTopics( dados );
  callSidConfig(dados.sensorId().c_str(), dados.subTopic().c_str());
}


void mqtt_callback(char* topic, byte* payload, unsigned int length) {
  Serial.println("---------------------------------------------");
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  Serial.println();
  
  int capacity = JSON_OBJECT_SIZE(10);
  DynamicJsonDocument doc(capacity);
  deserializeJson( doc, payload );

  if ( topic == config_topic( wChiller ) ) { //R1_IN1, S1
    if ( doc[ "active" ] == 1 ) {
      S1 = LOW;
    } else {
      S1 = HIGH;
    }
    digitalWrite( R1_IN1, S1 );
  } else if ( topic == config_topic( wHeater ) ) { //R1_IN2, S2
    if ( doc["active"] == 1 ) {
      S2 = LOW;
    } else {
      S2 = HIGH;
    }
    digitalWrite( R1_IN2, S2 );
  } else if ( topic == config_topic( wPump ) ) { //R1_IN3, S3
    if ( doc["active"] == 1 ) {
      S3 = LOW;
    } else {
      S3 = HIGH;
    }
    digitalWrite( R1_IN3, S3 );
  } else if ( topic == config_topic( lights ) ) { //R1_IN4, S4
    if ( doc["active"] == 1 ) {
      S4 = LOW;
    } else {
      S4 = HIGH;
    }
    digitalWrite( R1_IN4, S4 );
  } else if ( topic == config_topic( aHeater ) ) { //R2_IN1, S5
    if ( doc["active"] == 1 ) {
      S5 = LOW;
    } else {
      S5 = HIGH;
    }
    digitalWrite( R2_IN1, S5 );
  } else if ( topic == config_topic( aChiller ) ) { //R2_IN2, S6
    if ( doc["active"] == 1 ) {
      S6 = LOW;
    } else {
      S6 = HIGH;
    }
    digitalWrite( R2_IN2, S6 );
  } else if ( topic == config_topic( sprinkler ) ) { //R2_IN3, S7
    if ( doc["active"] == 1 ) {
      S7 = LOW;
    } else {
      S7 = HIGH;
    }
    digitalWrite( R2_IN3, S7 );
  } else {
    l_delay = doc[ "millisToVerify" ];
  }
}
