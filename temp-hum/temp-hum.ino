/* How to use the DHT-22 sensor with Arduino uno
   Temperature and humidity sensor
*/

//Libraries
#include <stdio.h>
#include <Sysponics.h>
#include <DHT.h>
#include <string>
#include <NodeMCU.h>

//ACCESS
#define USER_ID "wdltdfrmbgmhnfaa"
#define SENSOR_ID "rszkhajoldyccmyw"

//Constants
#define DHTPIN D3    // what pin we're connected to
#define DATALED D2

#define DHTTYPE DHT22   // DHT 22  (AM2302)

long l_delay = 10000;

DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino

void setup(){
  Serial.begin(9600);
  reconnectWifi( getWiFiData() ); 
  Serial.println( WiFi.localIP() );
  mqttSetup( getMQTTData() );
  pinMode( DATALED, OUTPUT );
  dht.begin();
}

void loop(){
    digitalWrite( DATALED, HIGH );
    if( !getMQTT().connected()){
      reconnectMQTT( getMQTTData() );
    }
    
    
    TempHumSensor thSensor;
    thSensor.setSensorId( SENSOR_ID );
    thSensor.setUserId( USER_ID );
    thSensor.setTemperature( dht.readTemperature() );
    thSensor.setHumidity( dht.readHumidity() );

    sendMessage(thSensor.toJson(), data_topic() );
    digitalWrite( DATALED, LOW );
    delay( l_delay );
}

std::string userId(){
  return USER_ID;
}

WifiConnectionData getWiFiData(){
  WifiConnectionData data;
  data.setSSID( "Hansen_Rep" );
  data.setPassword( "nicoli2010" );
  return data;
}

MQTTConnectionData getMQTTData(){
  
//  data.setApiKey( USER_ID );
//  data.setSensorType( WATER_TEMP_SENSOR );
//  data.setSensorId( SENSOR_ID );
  std::string idf = userId() + "@";
  idf += WiFi.localIP().toString().c_str();
  Serial.println( idf.c_str() );
//  data.setIdentifier( idf.c_str() );
  MQTTConnectionData data(USER_ID, AIR_TEMP_HUM_SENSOR, SENSOR_ID, idf );
  return data;
}

void mqtt_callback(char* topic, byte* payload, unsigned int length){
    Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
 
  Serial.println();
  Serial.println("-----------------------");
}
