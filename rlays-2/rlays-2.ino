#include <stdio.h>
#include <Sysponics.h>
#include <NodeMCU.h>

#define IDENTIFIER "20201115"
#define BROKER "192.168.2.104"
#define BROKER_PORT 1883


#define R1_IN1 D1
#define R1_IN2 D2
#define R1_IN3 D3
#define R1_IN4 D4
#define R2_IN1 D5
#define R2_IN2 D6
#define R2_IN3 D7
#define R2_IN4 D8
//
#define USER_ID "wdltdfrmbgmhnfaa"
#define K1 "1cdd2e7b97134ce5"
#define K2 "b81319dde3cc476b"
#define K3 "90142e537f04415a"
#define K4 "0dd322a6640d430b"
#define K5 "2770213aebc14f1f"
#define K6 "c012de6bec2a4d1d"
#define K7 "c556ad2eefdc44be"

MQTTConnectionData wChiller( std::string( USER_ID ), std::string( ACT_WATER_CHILLER ), K1, std::string( IDENTIFIER ) );
MQTTConnectionData wHeater( std::string( USER_ID ), std::string( ACT_WATER_HEATER ), K2, std::string( IDENTIFIER ) );
MQTTConnectionData wPump( std::string( USER_ID ), std::string( ACT_WATER_PUMP ), K3, std::string( IDENTIFIER ) );
MQTTConnectionData lights( std::string( USER_ID ), std::string( ACT_LIGHTS ), K4, std::string( IDENTIFIER ) );
MQTTConnectionData aHeater( std::string( USER_ID ), std::string( ACT_AIR_HEATER ), K5, std::string( IDENTIFIER ) );
MQTTConnectionData aChiller( std::string( USER_ID ), std::string( ACT_AIR_CHILLER ), K6, std::string( IDENTIFIER ) );
MQTTConnectionData sprinkler( std::string( USER_ID ), std::string( ACT_SPRINKLER ), K7, std::string( IDENTIFIER ) );

int S1 = LOW;
int S2 = LOW;
int S3 = LOW;
int S4 = LOW;
int S5 = LOW;
int S6 = LOW;
int S7 = LOW;
int S8 = LOW;
long l_delay = 10000;


WiFiClient wifi;
PubSubClient mqtt(wifi);

void setupWifi();
void setupMqtt();
void mqttCallback(char* topic, byte* payload, unsigned int length);

void sendState( std::string device, int state );

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode( R1_IN1, OUTPUT );
  pinMode( R1_IN2, OUTPUT );
  pinMode( R1_IN3, OUTPUT );
  pinMode( R1_IN4, OUTPUT );
  pinMode( R2_IN1, OUTPUT );
  pinMode( R2_IN2, OUTPUT );
  pinMode( R2_IN3, OUTPUT );
  pinMode( R2_IN4, OUTPUT );
  setupWifi();
  setupMqtt();

  digitalWrite( R1_IN1, LOW );
  digitalWrite( R1_IN2, LOW );
  digitalWrite( R1_IN3, LOW );
  digitalWrite( R1_IN4, LOW );
  digitalWrite( R2_IN1, LOW );
  digitalWrite( R2_IN2, LOW );
  digitalWrite( R2_IN3, LOW );
  digitalWrite( R2_IN4, LOW );
}

void loop() {
  // put your main code here, to run repeatedly:
  if ( !mqtt.connected() ) {
    Serial.println("RECONNECT");
    setupMqtt();
  }
  mqtt.loop();

  sendState(K1, S1);
  sendState(K2, S2);
  sendState(K3, S3);
  sendState(K4, S4);
  sendState(K5, S5);
  sendState(K6, S6);
  sendState(K7, S7);
  mqtt.loop();
  delay(10000);
}

void setupWifi() {
  if ( WiFi.status() == WL_CONNECTED ) {
    return;
  }
  Serial.println("CONNECTING TO WIFI");
  WiFi.begin("Hansen_Rep", "nicoli2010" );
  while ( WiFi.status() != WL_CONNECTED ) {
    delay(100);
  }
  Serial.println("CONNECTED TO WIFI");
}

void setupMqtt() {
  while ( !mqtt.connected() ) {
    Serial.println("CONNECTING TO MQTT");
    mqtt.setServer( BROKER, BROKER_PORT );
    mqtt.setCallback( mqttCallback );


    std::string identifier = "wdltdfrmbgmhnfaa@";
    identifier += WiFi.localIP().toString().c_str();
    Serial.println( identifier.c_str() );
    if ( mqtt.connect( identifier.c_str(), "pnktyzmy", "5fzaDbMmpFxN" ) ) {
      Serial.println( "Conectado ao MQTT" );
      if ( mqtt.subscribe("sysponics/user/wdltdfrmbgmhnfaa/config/+/return" ) ) {
        Serial.println("SUBSCRIBED TO sysponics/user/wdltdfrmbgmhnfaa/config/+/return" );
        sendConfig();
      } else {
        Serial.println("FAIL TO SUBSCRIBE" );
      }
    } else {
      Serial.println("Falha na conexão com o broker.");
      Serial.println("Tentando novamente em 2s." );
      delay(2000);
    }
  }
}

void sendConfig() {
  Serial.println("--------------------SEND CONFIG START-------------------------");
  std::string topic = "sysponics/user/wdltdfrmbgmhnfaa/config/hardware";
  int capacity = JSON_OBJECT_SIZE(3);
  DynamicJsonDocument doc(capacity);
  doc["user_id"] = USER_ID;
  char buffer[512];
  size_t n = serializeJson(doc, buffer);
  char c_topic[topic.size() + 1];
  strcpy(c_topic, topic.c_str());
  if (mqtt.publish(c_topic, buffer, n)) {
    Serial.println("Config sent to broker");
  } else {
    Serial.println("Erro ao publicar mensagem");
  }
  Serial.println("--------------------SEND CONFIG END---------------------------");
}


void sendState( std::string device, int state ) {
  Serial.println("--------------------SEND DATA START---------------------------");
  std::string topic = "sysponics/user/wdltdfrmbgmhnfaa/data";
  int capacity = JSON_OBJECT_SIZE(3);
  DynamicJsonDocument doc(capacity);
  doc["sensor_id"] = device.c_str();
  doc["low"] = state;
  doc["user_id"] = USER_ID;
  char buffer[512];
  size_t n = serializeJson(doc, buffer);
  char c_topic[topic.size() + 1];
  strcpy(c_topic, topic.c_str());
  if (mqtt.publish(c_topic, buffer, n)) {
    Serial.println("Data sent to broker");
    Serial.print("TOPIC:");
    Serial.println( topic.c_str() );
  } else {
    Serial.println("Erro ao publicar mensagem");
  }
  Serial.println("--------------------SEND DATA END-----------------------------");
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {
  Serial.println("--------------------MESSAGE START-----------------------------");

  Serial.println(topic);
  std::string sTopic( topic );
  Serial.println(sTopic.c_str());
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  int capacity = JSON_OBJECT_SIZE(10);
  DynamicJsonDocument doc(capacity);
  deserializeJson( doc, payload );
  if ( sTopic == "sysponics/user/wdltdfrmbgmhnfaa/config/ACT_WATER_CHILLER/return" ) { //R1_IN1, S1
    if ( doc[ "active" ] == 1 ) {
      S1 = HIGH;
    } else {
      S1 = LOW;
    }
    digitalWrite( R1_IN1, S1 );
  } else if ( sTopic == "sysponics/user/wdltdfrmbgmhnfaa/config/ACT_WATER_HEATER/return" ) { //R1_IN2, S2
    if ( doc["active"] == 1 ) {
      S2 = HIGH;
    } else {
      S2 = LOW;
    }
    digitalWrite( R1_IN2, S2 );
  } else if ( sTopic == "sysponics/user/wdltdfrmbgmhnfaa/config/ACT_WATER_PUMP/return" ) { //R1_IN3, S3
    if ( doc["active"] == 1 ) {
      S3 = HIGH;
    } else {
      S3 = LOW;
    }
    digitalWrite( R1_IN3, S3 );
  } else if ( sTopic == "sysponics/user/wdltdfrmbgmhnfaa/config/ACT_LIGHTS/return" ) { //R1_IN4, S4
    if ( doc["active"] == 1 ) {
      S4 = HIGH;
    } else {
      S4 = LOW;
    }
    digitalWrite( R1_IN4, S4 );
  } else if ( sTopic == "sysponics/user/wdltdfrmbgmhnfaa/config/ACT_AIR_HEATER/return" ) { //R2_IN1, S5
    if ( doc["active"] == 1 ) {
      S5 = HIGH;
    } else {
      S5 = LOW;
    }
    digitalWrite( R2_IN1, S5 );
  } else if ( sTopic == "sysponics/user/wdltdfrmbgmhnfaa/config/ACT_AIR_CHILLER/return" ) { //R2_IN2, S6
    if ( doc["active"] == 1 ) {
      S6 = HIGH;
    } else {
      S6 = LOW;
    }
    digitalWrite( R2_IN2, S6 );
  } else if ( sTopic == "sysponics/user/wdltdfrmbgmhnfaa/config/ACT_SPRINKLER/return" ) { //R2_IN3, S7
    if ( doc["active"] == 1 ) {
      S7 = HIGH;
    } else {
      S7 = LOW;
    }
    digitalWrite( R2_IN3, S7 );
  } else {
    //l_delay = doc[ "millisToVerify" ];
  }
  Serial.println("--------------------MESSAGE END-------------------------------");
}

std::string userId() {
  return USER_ID;
}
