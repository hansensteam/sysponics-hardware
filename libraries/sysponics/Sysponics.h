//
// Created by Francisco on 31/08/2019.
//
#ifndef SYSPONICS_SYSPONICS_H
#define SYSPONICS_SYSPONICS_H

#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <string>

#define WATER_TEMP_SENSOR "WATER_TEMP_SENSOR"
#define WATER_PH_SENSOR "WATER_PH_SENSOR"
#define WATER_CE_SENSOR "WATER_CE_SENSOR"
#define AIR_TEMP_SENSOR "AIR_TEMP_SENSOR"
#define AIR_HUM_SENSOR "AIR_HUM_SENSOR"
#define AIR_TEMP_HUM_SENSOR "AIR_TEMP_HUM_SENSOR"
#define ACT_LIGHTS "ACT_LIGHTS"
#define ACT_WATER_PUMP "ACT_WATER_PUMP"
#define ACT_AIR_PUMP "ACT_AIR_PUMP"
#define ACT_FAN "ACT_FAN"
#define ACT_WATER_CHILLER "ACT_WATER_CHILLER"
#define ACT_AIR_CHILLER "ACT_AIR_CHILLER"
#define ACT_WATER_HEATER "ACT_WATER_HEATER"
#define ACT_AIR_HEATER "ACT_AIR_HEATER"
#define ACT_SPRINKLER "ACT_SPRINKLER"

#define LIGHTS "LIGHTS"
#define WATER_TEMP "WATER_TEMP"
#define AIR_TEMP "AIR_TEMP"
#define AIR_HUM "AIR_HUM"

#define WATER_LEVEL_SENSOR "WATER_LEVEL_SENSOR"


WiFiClient getClient();

PubSubClient getMQTT();

std::string userId();

std::string data_topic();


void mqtt_callback(char *topic, byte *payload, unsigned int length);

/**
 * --------------------------- Model ------------------------------
 */
class SensorData {
private:
    long id;
    std::string d_userId;
    std::string d_sensorId;
public:
    void setUserId(std::string const &uid);

    void setSensorId(std::string const &sid);


    std::string const &userId() const;

    std::string const &sensorId() const;

};

class TempSensor : public SensorData {
private:
    float d_temperature;
public:
    float temperature() const;

    void setTemperature(float temp);

    DynamicJsonDocument toJson();
};

class TempHumSensor : public SensorData {
private:
    float d_temperature;
    float d_humidity;
public:
    void setTemperature(float temp);

    void setHumidity(float hum);

    float temperature() const;

    float humidity() const;

    DynamicJsonDocument toJson();
};

class Configuration {
private:
    long l_millisToVerify;
    long l_pumpTimeOn;
    long l_pumpTimeOff;
    long l_lightsTimeOn;
    long l_lightsTimeOff;
public:
    void setMillisToVerify(long millisToVerify);

    void setPumpTimeOn(long pumpTimeOn);

    void setPumpTimeOff(long pumpTimeOff);

    void setLightsTimeOn(long lightsTimeOn);

    void setLightsTimeOff(long lightsTimeOff);

    long millisToVerify() const;

    long pumpTimeOn() const;

    long lightsTimeOn() const;

    long pumpTimeOff() const;

    long lightsTimeOff() const;
};


class Dev_Settings {
private:
    long l_millisToVerify;
    float l_ph;
public:
    void setMillisToVerify(long d);

    void setPh(float pH);

    long millisToVerify() const;

    float pH() const;

    DynamicJsonDocument toJson();
};

class Crop_Schedules {
private:
    long l_start;
    long l_end;
    float l_value;
    bool l_on;
public:
    void setStart(long start);

    void setEnd(long end);

    void setValue(float value);

    void setOn(bool on);

    long start() const;

    long end() const;

    float value() const;

    bool on() const;
};

class LevelSensor : public SensorData {
private:
    int d_low;
public:
    void setLow(int x);

    int isLow() const;

    DynamicJsonDocument toJson();
};

/**
 * --------------------------- Connection ------------------------------
 */

class WifiConnectionData {
private:
    std::string d_ssid;
    std::string d_password;
public:
    void setSSID(std::string const &sid);

    void setPassword(std::string const &pwd);

    std::string const &ssid() const;

    std::string const &password() const;
};

class MQTTConnectionData {
private:
    std::string d_broker = "192.168.2.104";
    int d_port = 1883;
    std::string d_apiKey;
    std::string d_sensorType;
    std::string d_identifier;
    std::string d_sensorId;
public:
    void setBroker(std::string const &custom_broker);

    void setPort(int p);

    void setApiKey(std::string const &key);

    void setSensorType(std::string const &&sType);

    void setIdentifier(std::string const &identifier);

    void setSensorId(std::string const &sensorId);

    std::string const &broker() const;

    int port() const;

    std::string const &apiKey() const;

    std::string const &sensorType() const;

    std::string const subTopic() const;

    std::string const identifier() const;

    std::string const sensorId() const;

    MQTTConnectionData( std::string const& apiKey, std::string const& sensorType, std::string const& sensorId, std::string const& identifier ){
        d_apiKey = apiKey;
        d_sensorType = sensorType;
        d_sensorId = sensorId;
        d_identifier = identifier;
    }
};

void subscribeToTopics(MQTTConnectionData data);

void publishToConfig(MQTTConnectionData data);

void callSidConfig(std::string sid, std::string topic);

void reconnectWifi(WifiConnectionData data);

void reconnectMQTT(MQTTConnectionData data);

void mqttSetup(MQTTConnectionData data);

void sendMessage(DynamicJsonDocument data, std::string const &topic);

int *splitMillis(long millis);

long hmsToMillis(int h, int m, int s);

std::string config_topic(MQTTConnectionData data);

#endif //SYSPONICS_SYSPONICS_H
