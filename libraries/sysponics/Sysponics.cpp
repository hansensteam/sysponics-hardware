//
// Created by Francisco on 31/08/2019.
//
#include "Sysponics.h"
#include<stdio.h>

WiFiClient wClient;
PubSubClient MQTT(wClient);


WiFiClient getClient() {
    return wClient;
}

PubSubClient getMQTT() {
    return MQTT;
}

/*
 * SensorData
 */

const std::string &SensorData::userId() const {
    return d_userId;
}

const std::string &SensorData::sensorId() const {
    return d_sensorId;
}

void SensorData::setSensorId(const std::string &sid) {
    d_sensorId = sid;
}

void SensorData::setUserId(const std::string &uid) {
    d_userId = uid;
}


/*
 * TempSensor
 */
void TempSensor::setTemperature(float temp) {
    d_temperature = temp;
}

float TempSensor::temperature() const {
    return d_temperature;
}

DynamicJsonDocument TempSensor::toJson() {
    int capacity = JSON_OBJECT_SIZE(4);
    DynamicJsonDocument doc(capacity);
    doc["user_id"] = userId().c_str();
    doc["sensor_id"] = sensorId().c_str();
    doc["temperature"] = d_temperature;
    return doc;
}

/*
 * TempHumSensor
 */

void TempHumSensor::setTemperature(float temp) {
    d_temperature = temp;
}

void TempHumSensor::setHumidity(float hum) {
    d_humidity = hum;
}

float TempHumSensor::temperature() const {
    return d_temperature;
}

float TempHumSensor::humidity() const {
    return d_humidity;
}

DynamicJsonDocument TempHumSensor::toJson() {
    int capacity = JSON_OBJECT_SIZE(5);
    DynamicJsonDocument doc(capacity);
    doc["temperature"] = d_temperature;
    doc["humidity"] = d_humidity;
    doc["sensor_id"] = sensorId().c_str();
    doc["user_id"] = userId().c_str();
    return doc;
}

void LevelSensor::setLow(int x) {
    d_low = x;
}

int LevelSensor::isLow() const {
    return d_low;
}

DynamicJsonDocument LevelSensor::toJson() {
    int capacity = JSON_OBJECT_SIZE(4);
    DynamicJsonDocument doc(capacity);
    doc["low"] = d_low;
    doc["sensor_id"] = sensorId().c_str();
    doc["user_id"] = userId().c_str();
    return doc;
}

/*
 * WifiConnectionData
 */
void WifiConnectionData::setSSID(const std::string &sid) {
    d_ssid = sid;
}

void WifiConnectionData::setPassword(const std::string &pwd) {
    d_password = pwd;
}

const std::string &WifiConnectionData::ssid() const {
    return d_ssid;
}

const std::string &WifiConnectionData::password() const {
    return d_password;
}

/*
 * MQTTConnectionData
 */
void MQTTConnectionData::setBroker(const std::string &custom_broker) {
    d_broker = custom_broker;
}

void MQTTConnectionData::setPort(int p) {
    d_port = p;
}

void MQTTConnectionData::setApiKey(const std::string &key) {
    d_apiKey = key;
}

const std::string &MQTTConnectionData::broker() const {
    return d_broker;
}

int MQTTConnectionData::port() const {
    return d_port;
}

const std::string &MQTTConnectionData::apiKey() const {
    return d_apiKey;
}

void MQTTConnectionData::setSensorType(const std::string &&sType) {
    d_sensorType = sType;
}

void MQTTConnectionData::setIdentifier(const std::string &identifier) {
    d_identifier = identifier;
}

void MQTTConnectionData::setSensorId(const std::string &sensorId) {
    d_sensorId = sensorId;
}

const std::string &MQTTConnectionData::sensorType() const {
    return d_sensorType;
}

const std::string MQTTConnectionData::subTopic() const {
    return "sysponics/user/" + userId() + "/config/" + sensorType();
}

const std::string MQTTConnectionData::identifier() const {
    return d_identifier;
}

const std::string MQTTConnectionData::sensorId() const {
    return d_sensorId;
}

void Dev_Settings::setMillisToVerify(long d) {
    l_millisToVerify = d;
}

void Dev_Settings::setPh(float pH) {
    l_ph = pH;
}

float Dev_Settings::pH() const {
    return l_ph;
}

long Dev_Settings::millisToVerify() const {
    return l_millisToVerify;
}

//-------------------------------------------------------

void reconnectWifi(WifiConnectionData data) {
    if (WiFi.status() == WL_CONNECTED) {
        return;
    }
    Serial.println("Connecting WiFi");
    WiFi.begin(data.ssid().c_str(), data.password().c_str());
    while (WiFi.status() != WL_CONNECTED) {
        delay(100);
    }
    Serial.println("Connected WiFi");
}

void publishToConfig(MQTTConnectionData data) {
    Serial.println("------------------GETTING CONFIG------------------");
//    char sid[sensorId().size() + 1];
//    strcpy(sid, sensorId().c_str());
    std::string g_topic = "sysponics/user/" + userId() + "/config/general";
    callSidConfig(data.sensorId().c_str(), g_topic.c_str());
//    char t_general[g_topic.size() + 1];
//    strcpy(t_general, g_topic.c_str());
//    Serial.println(t_general);
//    MQTT.publish(t_general, sid);
    callSidConfig(data.sensorId().c_str(), data.subTopic().c_str());
//    char topic[data.subTopic().size() + 1];
//    strcpy(topic, data.subTopic().c_str());
//    MQTT.publish(topic, sid);
    Serial.println("------------------END GET CONFIG------------------");
}

void callSidConfig(std::string sid, std::string topic) {
    char p_topic[topic.size() + 1];
    char p_sid[sid.size() + 1];
    strcpy(p_sid, sid.c_str());
    strcpy(p_topic, topic.c_str());
    MQTT.publish(p_topic, p_sid);
}

void subscribeToTopics( MQTTConnectionData data ){
    Serial.println("------------------SUBSCRIBING------------------");
    char t_data[data.subTopic().size() + 1];
    strcpy(t_data, data.subTopic().c_str());
    if (MQTT.subscribe(t_data)) {
        Serial.print(t_data);
        Serial.print(".");
        Serial.println();
    } else {
        Serial.print("FAIL:::");
        Serial.print(t_data);
        Serial.println();
    }
    char t_config[config_topic(data).size() + 1];
    strcpy(t_config, config_topic(data).c_str());

    if (MQTT.subscribe(t_config)) {
        Serial.println(t_config);
    } else {
        Serial.print("FAIL:::");
        Serial.print(t_config);
        Serial.println();
    }
    Serial.println("------------------END SUBSCRIBE------------------");
}

void reconnectMQTT(MQTTConnectionData data) {
    while (!MQTT.connected()) {
        Serial.println("Connecting to MQTT");
        char broker[data.broker().size() + 1];
        strcpy(broker, data.broker().c_str());
        Serial.println(data.broker().c_str());
        MQTT.setServer(broker, data.port());
        Serial.println("BEFORE SET CALLBACK");
        MQTT.setCallback(mqtt_callback);
        Serial.println("AFTER SET CALLBACK");
        if (MQTT.connect(data.identifier().c_str(), "pnktyzmy", "5fzaDbMmpFxN")) {
            Serial.println("Connected to MQTT");
            subscribeToTopics( data );
            publishToConfig(data);
        } else {
            Serial.println("Falha ao reconectar no broker.");
            Serial.println("Havera nova tentatica de conexao em 2s");
            delay(2000);
        }
    }
}

std::string data_topic() {
    return "sysponics/user/" + userId() + "/data";
}

std::string config_topic(MQTTConnectionData data ) {
    return "sysponics/user/" + userId() + "/config/" + data.sensorId() + "/return";
}

void mqttSetup(MQTTConnectionData data) {
    Serial.println();
    Serial.print("Connecting to MQTT on ");
    Serial.print(data.broker().c_str());
    Serial.print(":");
    Serial.print(data.port());
    Serial.println();
    char broker[data.broker().size() + 1];
    strcpy(broker, data.broker().c_str());
    MQTT.setServer(broker, data.port());   //informa qual broker e porta deve ser conectado
    MQTT.setCallback(mqtt_callback);
}


void sendMessage(DynamicJsonDocument data, std::string const &topic) {
    char buffer[512];
    size_t n = serializeJson(data, buffer);
    char c_topic[topic.size() + 1];
    strcpy(c_topic, topic.c_str());
//    Serial.println(c_topic);
//    Serial.println(buffer);
    if (getMQTT().publish(c_topic, buffer, n)) {
//        Serial.println("Data sent to broker");
    } else {
        Serial.println("Erro ao publicar mensagem");
    }
}


void Configuration::setLightsTimeOff(long lightsTimeOff) {
    l_lightsTimeOff = lightsTimeOff;
}

void Configuration::setLightsTimeOn(long lightsTimeOn) {
    l_lightsTimeOn = lightsTimeOn;
}

void Configuration::setMillisToVerify(long millisToVerify) {
    l_millisToVerify = millisToVerify;
}

void Configuration::setPumpTimeOn(long pumpTimeOn) {
    l_pumpTimeOn = pumpTimeOn;
}

void Configuration::setPumpTimeOff(long pumpTimeOff) {
    l_pumpTimeOff = pumpTimeOff;
}

long Configuration::millisToVerify() const {
    return l_millisToVerify;
}

long Configuration::pumpTimeOn() const {
    return l_pumpTimeOn;
}

long Configuration::pumpTimeOff() const {
    return l_pumpTimeOff;
}

long Configuration::lightsTimeOn() const {
    return l_lightsTimeOn;
}

long Configuration::lightsTimeOff() const {
    return l_lightsTimeOff;
}



int *splitMillis(long millis) {
    int splitted[3];
    splitted[0] = (millis / 1000) % 60;
    splitted[1] = (millis / (1000 * 60) % 60);
    splitted[2] = (millis / (1000 * 60 * 60)) % 24;
    return splitted;
}

long hmsToMillis(int h, int m, int s) {
    return ((h * 60 * 60) + (m * 60) + s) * 1000;
}



