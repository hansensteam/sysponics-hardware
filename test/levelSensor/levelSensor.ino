#include<stdio.h>
#include<Sysponics.h>
#include <DHT.h>
#include <string>

#define SLEVEL D6


void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
  pinMode( SLEVEL, INPUT_PULLUP );
}

void loop() {
  // put your main code here, to run repeatedly:
  int x = digitalRead( SLEVEL );
  Serial.println( x );
  delay( 1000 );
}
