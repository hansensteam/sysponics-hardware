#include<stdio.h>
#include<Sysponics.h>
#include <DHT.h>

#define DHTPIN D6

void setup() {
  pinMode( DHTPIN, OUTPUT );
  // put your setup code here, to run once:

}

void loop() {
  digitalWrite( DHTPIN, HIGH );
  delay( 1000 );
  digitalWrite( DHTPIN, LOW );
  delay( 1000 );
  // put your main code here, to run repeatedly:

}
