#include<stdio.h>
#include<Sysponics.h>
#include <DHT.h>
#include <string>

#define RELAY1 D0

void setup() {
  Serial.begin(9600);
  pinMode( RELAY1, OUTPUT );
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite( RELAY1, HIGH );
  Serial.println("HIGH");
  delay( 1000 );
  digitalWrite( RELAY1, LOW );
  Serial.println("LOW" );
  delay( 1000 );
}
